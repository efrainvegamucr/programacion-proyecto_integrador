/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JPanel.java to edit this template
 */
package Vista;

import Controlador.ControladorEmpleados;
import Modelo.Usuario;
import java.util.List;
import javax.swing.DefaultComboBoxModel;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author efrai
 */
public class PanelEmpleados extends javax.swing.JPanel {

    /**
     * Creates new form PanelUsuarios
     */
    public PanelEmpleados() {
        initComponents();
        limpiar();
    }

    public void limpiar() {
        txtNombre.setText("");
        txtPrimerApellido.setText("");
        txtSegundoApellido.setText("");
        txtCedula.setText("");
        txtFechaIngreso.setText("");
        txtSalarioBase.setText("");
        txtTelefonoFijo.setText("");
        txtTelefonoMovil.setText("");
        txtCorreo.setText("");
    }

    public void setDatosTabla(String[][] datos, String[] etiquetas) {
        this.jTRegistroEmpleados.setModel(new DefaultTableModel(datos, etiquetas));
        this.jScrollPane2.setViewportView(this.jTRegistroEmpleados);
    }

    public void escuchar(ControladorEmpleados controladorEmpleados) {
        btnConsultar.addActionListener(controladorEmpleados);
        btnAgregar.addActionListener(controladorEmpleados);
        btnModificar.addActionListener(controladorEmpleados);
        btnEliminar.addActionListener(controladorEmpleados);
        btnSalir.addActionListener(controladorEmpleados);
    }

    public void escucharTabla(ControladorEmpleados controladorEmpleados) {
        jTRegistroEmpleados.addMouseListener(controladorEmpleados);
    }

    public void setjCArea_Desempeño(String[] listaAreasDesempeño) {
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel();
        comboModel.addElement("-");

        for (int indice = 0; indice < listaAreasDesempeño.length; indice++) {
            comboModel.addElement(listaAreasDesempeño[indice]);
        }
        jCArea_Desempeño.setModel(comboModel);
    }

    public String getjCArea_Desempeño() {
        return jCArea_Desempeño.getSelectedItem().toString();
    }

    public String getFila() {
        int fila = jTRegistroEmpleados.getSelectedRow();
        return jTRegistroEmpleados.getModel().getValueAt(fila, 0).toString();
    }

    public void setTxtNombre(String nombre) {
        txtNombre.setText(nombre);
    }

    public String getTxtNombre() {
        return txtNombre.getText().trim();
    }

    public void setTxtPrimerApellido(String primerApellido) {
        txtPrimerApellido.setText(primerApellido);
    }

    public String getTxtPrimerApellido() {
        return txtPrimerApellido.getText().trim();
    }

    public void setTxtSegundoApellido(String segundoApellido) {
        txtSegundoApellido.setText(segundoApellido);
    }

    public String getTxtSegundoApellido() {
        return txtSegundoApellido.getText().trim();
    }

    public void setTxtCedula(int cedula) {
        txtCedula.setText(Integer.toString(cedula));
    }

    public int getTxtCedula() {
        int cedula = -1;
        try {//Agarra el string del setTxtAltura
            cedula = Integer.parseInt(txtCedula.getText().trim());
        } catch (Exception e) {//el catch hagarra el error y devuelve 0
            cedula = 0;
        }
        return cedula;
    }

    public void setTxtFechaIngreso(String fecha) {
        txtFechaIngreso.setText(fecha);
    }

    public String getTxtFechaIngreso() {
        return txtFechaIngreso.getText().trim();
    }

    public void setTxtSalarioBase(double salario) {
        txtSalarioBase.setText(Double.toString(salario));
    }

    public double getTxtSalarioBase() {
        double salario = -1;
        try {//hagarra el string del setTxtAltura
            salario = Double.parseDouble(txtSalarioBase.getText().trim());

        } catch (Exception e) {//el catch hagarra el error y devuelve 0
            salario = 0;
        }
        return salario;
    }

    public void setTxtTelefonoFijo(String telefonoFijo) {
        txtTelefonoFijo.setText(telefonoFijo);
    }

    public String getTxtTelefonoFijo() {
        return txtTelefonoFijo.getText().trim();
    }

    public void setTxtTelefonoMovil(String telefonoMovil) {
        txtTelefonoMovil.setText(telefonoMovil);
    }

    public String getTxtTelefonoMovil() {
        return txtTelefonoMovil.getText().trim();
    }

    public void setTxtCorreo(String correo) {
        txtCorreo.setText(correo);
    }

    public String getTxtCorreo() {
        return txtCorreo.getText().trim();
    }

    public void setjCAisgnar_Usuario(List<Usuario> listaUsuarios) {
        DefaultComboBoxModel comboModel = new DefaultComboBoxModel();
        comboModel.addElement("-");

        for (int indice = 0; indice < listaUsuarios.size(); indice++) {
            comboModel.addElement(listaUsuarios.get(indice).getNombreUsuario());
        }
        jCAsignar_Usuario.setModel(comboModel);
    }

    public String getjCAisgnar_Usuario() {
        return jCAsignar_Usuario.getSelectedItem().toString();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbFechaIngreso = new javax.swing.JLabel();
        txtTelefonoFijo = new javax.swing.JTextField();
        txtTelefonoMovil = new javax.swing.JTextField();
        lbSalarioBase = new javax.swing.JLabel();
        txtCorreo = new javax.swing.JTextField();
        lbTelefonoFijo = new javax.swing.JLabel();
        lbTelefonoMovil = new javax.swing.JLabel();
        lbCorreo = new javax.swing.JLabel();
        lbCedula = new javax.swing.JLabel();
        btnAgregar = new javax.swing.JButton();
        lbPrimerApellido = new javax.swing.JLabel();
        btnConsultar = new javax.swing.JButton();
        lbSegundoApellido = new javax.swing.JLabel();
        btnModificar = new javax.swing.JButton();
        btnEliminar = new javax.swing.JButton();
        txtNombre = new javax.swing.JTextField();
        txtPrimerApellido = new javax.swing.JTextField();
        txtSegundoApellido = new javax.swing.JTextField();
        txtCedula = new javax.swing.JTextField();
        jCArea_Desempeño = new javax.swing.JComboBox<>();
        lbTitulo = new javax.swing.JLabel();
        txtFechaIngreso = new javax.swing.JTextField();
        lbArea_Desempeño = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jTRegistroEmpleados = new javax.swing.JTable();
        lbNombre = new javax.swing.JLabel();
        txtSalarioBase = new javax.swing.JTextField();
        btnSalir = new javax.swing.JButton();
        lbAsignar_Usuario = new javax.swing.JLabel();
        jCAsignar_Usuario = new javax.swing.JComboBox<>();
        Fondo = new javax.swing.JLabel();

        setMinimumSize(new java.awt.Dimension(1280, 720));
        setPreferredSize(new java.awt.Dimension(1280, 720));
        setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        lbFechaIngreso.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbFechaIngreso.setForeground(new java.awt.Color(51, 153, 255));
        lbFechaIngreso.setText("Fecha de Ingreso:");
        add(lbFechaIngreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 190, -1, -1));

        txtTelefonoFijo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtTelefonoFijo, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 160, 190, -1));

        txtTelefonoMovil.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtTelefonoMovil, new org.netbeans.lib.awtextra.AbsoluteConstraints(810, 190, 180, -1));

        lbSalarioBase.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbSalarioBase.setForeground(new java.awt.Color(51, 153, 255));
        lbSalarioBase.setText("Salario Base:");
        add(lbSalarioBase, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 220, -1, -1));

        txtCorreo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 220, 210, -1));

        lbTelefonoFijo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbTelefonoFijo.setForeground(new java.awt.Color(51, 153, 255));
        lbTelefonoFijo.setText("Teléfono Fijo:");
        add(lbTelefonoFijo, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 160, -1, -1));

        lbTelefonoMovil.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbTelefonoMovil.setForeground(new java.awt.Color(51, 153, 255));
        lbTelefonoMovil.setText("Teléfono Movil:");
        add(lbTelefonoMovil, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 190, -1, -1));

        lbCorreo.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbCorreo.setForeground(new java.awt.Color(51, 153, 255));
        lbCorreo.setText("Correo Institucional:");
        add(lbCorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(670, 220, -1, -1));

        lbCedula.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbCedula.setForeground(new java.awt.Color(51, 153, 255));
        lbCedula.setText("Cédula:");
        add(lbCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 160, -1, -1));

        btnAgregar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnAgregar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Agregar.png"))); // NOI18N
        btnAgregar.setActionCommand("Agregar");
        btnAgregar.setContentAreaFilled(false);
        btnAgregar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RolloverIcon/AgregarR.png"))); // NOI18N
        add(btnAgregar, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 630, -1, -1));

        lbPrimerApellido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbPrimerApellido.setForeground(new java.awt.Color(51, 153, 255));
        lbPrimerApellido.setText("Primer Apellido:");
        add(lbPrimerApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 190, -1, -1));

        btnConsultar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnConsultar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Consultar.png"))); // NOI18N
        btnConsultar.setActionCommand("Consultar");
        btnConsultar.setContentAreaFilled(false);
        btnConsultar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RolloverIcon/ConsultarR.png"))); // NOI18N
        add(btnConsultar, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 630, -1, -1));

        lbSegundoApellido.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbSegundoApellido.setForeground(new java.awt.Color(51, 153, 255));
        lbSegundoApellido.setText("Segundo Apellido:");
        add(lbSegundoApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 220, -1, -1));

        btnModificar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnModificar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Modificar.png"))); // NOI18N
        btnModificar.setActionCommand("Modificar");
        btnModificar.setContentAreaFilled(false);
        btnModificar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RolloverIcon/ModificarR.png"))); // NOI18N
        add(btnModificar, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 630, -1, -1));

        btnEliminar.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnEliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Eliminar.png"))); // NOI18N
        btnEliminar.setActionCommand("Eliminar");
        btnEliminar.setContentAreaFilled(false);
        btnEliminar.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RolloverIcon/EliminarR.png"))); // NOI18N
        add(btnEliminar, new org.netbeans.lib.awtextra.AbsoluteConstraints(790, 630, -1, -1));

        txtNombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 160, 180, -1));

        txtPrimerApellido.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtPrimerApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(180, 190, 141, -1));

        txtSegundoApellido.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtSegundoApellido, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 220, 125, -1));

        txtCedula.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtCedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 160, 210, -1));

        jCArea_Desempeño.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jCArea_Desempeño.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Soporte", "Gerencia de Cajas", "Gerencia de Atencion al Cliente", "Gerencia General", "Gerencia de Préstamos", "Cajero", "Atención al Cliente", "Analista de Préstamos", "Recursos Humanos" }));
        add(jCArea_Desempeño, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 180, 202, -1));

        lbTitulo.setFont(new java.awt.Font("Tahoma", 1, 48)); // NOI18N
        lbTitulo.setForeground(new java.awt.Color(51, 153, 255));
        lbTitulo.setText("Administración y Mantenimiento de Empleados");
        add(lbTitulo, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, -1, -1));

        txtFechaIngreso.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtFechaIngreso, new org.netbeans.lib.awtextra.AbsoluteConstraints(510, 190, 130, -1));

        lbArea_Desempeño.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbArea_Desempeño.setForeground(new java.awt.Color(51, 153, 255));
        lbArea_Desempeño.setText("Area de Desempeño:");
        add(lbArea_Desempeño, new org.netbeans.lib.awtextra.AbsoluteConstraints(1080, 150, -1, -1));

        jTRegistroEmpleados.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null}
            },
            new String [] {
                "Cedula", "Nombre", "Primer Apellido", "Segundo Apellido", "Area de Desempeño"
            }
        ));
        jScrollPane2.setViewportView(jTRegistroEmpleados);

        add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 300, 1090, 310));

        lbNombre.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbNombre.setForeground(new java.awt.Color(51, 153, 255));
        lbNombre.setText("Nombre:");
        add(lbNombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 160, -1, -1));

        txtSalarioBase.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(txtSalarioBase, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 220, 166, -1));

        btnSalir.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        btnSalir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Salir.png"))); // NOI18N
        btnSalir.setActionCommand("Salir");
        btnSalir.setContentAreaFilled(false);
        btnSalir.setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/RolloverIcon/SalirR.png"))); // NOI18N
        add(btnSalir, new org.netbeans.lib.awtextra.AbsoluteConstraints(1020, 630, -1, -1));

        lbAsignar_Usuario.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        lbAsignar_Usuario.setForeground(new java.awt.Color(51, 153, 255));
        lbAsignar_Usuario.setText("Asignar Usuario:");
        add(lbAsignar_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(1100, 220, -1, -1));

        jCAsignar_Usuario.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        add(jCAsignar_Usuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(1060, 250, 202, -1));

        Fondo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Fondo_1280x720_(Empleado y Usuario).png"))); // NOI18N
        add(Fondo, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, -1, -1));
    }// </editor-fold>//GEN-END:initComponents


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel Fondo;
    private javax.swing.JButton btnAgregar;
    private javax.swing.JButton btnConsultar;
    private javax.swing.JButton btnEliminar;
    private javax.swing.JButton btnModificar;
    private javax.swing.JButton btnSalir;
    private javax.swing.JComboBox<String> jCArea_Desempeño;
    private javax.swing.JComboBox<String> jCAsignar_Usuario;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTable jTRegistroEmpleados;
    private javax.swing.JLabel lbArea_Desempeño;
    private javax.swing.JLabel lbAsignar_Usuario;
    private javax.swing.JLabel lbCedula;
    private javax.swing.JLabel lbCorreo;
    private javax.swing.JLabel lbFechaIngreso;
    private javax.swing.JLabel lbNombre;
    private javax.swing.JLabel lbPrimerApellido;
    private javax.swing.JLabel lbSalarioBase;
    private javax.swing.JLabel lbSegundoApellido;
    private javax.swing.JLabel lbTelefonoFijo;
    private javax.swing.JLabel lbTelefonoMovil;
    private javax.swing.JLabel lbTitulo;
    private javax.swing.JTextField txtCedula;
    private javax.swing.JTextField txtCorreo;
    private javax.swing.JTextField txtFechaIngreso;
    private javax.swing.JTextField txtNombre;
    private javax.swing.JTextField txtPrimerApellido;
    private javax.swing.JTextField txtSalarioBase;
    private javax.swing.JTextField txtSegundoApellido;
    private javax.swing.JTextField txtTelefonoFijo;
    private javax.swing.JTextField txtTelefonoMovil;
    // End of variables declaration//GEN-END:variables
}
