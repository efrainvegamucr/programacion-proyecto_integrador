/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/GUIForms/JFrame.java to edit this template
 */
package Vista;

import Controlador.ControladorEmpleados;
import Modelo.Usuario;
import java.util.List;
import javax.swing.JOptionPane;

/**
 *
 * @author efrai
 */
public class GUIEmpleados extends javax.swing.JFrame {

    private ControladorEmpleados controladorEmpleados;
    public GUIOpciones guiOpciones;

    /**
     * Creates new form GUIEmpleados
     */
    public GUIEmpleados(GUIOpciones guiOpciones) {
        initComponents();
        controladorEmpleados = new ControladorEmpleados(this, panelEmpleados);
        panelEmpleados.escuchar(controladorEmpleados);
        panelEmpleados.escucharTabla(controladorEmpleados);
        this.guiOpciones = guiOpciones;
        setResizable(false);
        setLocationRelativeTo(null);
    }

    public void setComboUsuarios(List<Usuario> listaUsuarios) {
        panelEmpleados.setjCAisgnar_Usuario(listaUsuarios);
    }

    public void mostrarMensaje(String mensaje) {
        JOptionPane.showMessageDialog(null, mensaje);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        panelEmpleados = new Vista.PanelEmpleados();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(panelEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(panelEmpleados, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    /**
     * @param args the command line arguments
     */
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private Vista.PanelEmpleados panelEmpleados;
    // End of variables declaration//GEN-END:variables
}
