/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package Vista;

import Modelo.Empleado;
import Modelo.EmpleadoJpaController;
import Modelo.Modulo;
import Modelo.ModuloJpaController;
import Modelo.Perfil;
import Modelo.PerfilJpaController;
import Modelo.Usuario;
import Modelo.UsuarioJpaController;
import javax.persistence.Persistence;

/**
 *
 * @author efrai
 */
public class Prueba {

    public static void main(String[] args) throws Exception {
        
//        PerfilJpaController jpaPerfil = new PerfilJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
//        jpaPerfil.create(new Perfil(222, "tipoUsuario3", "Descripcion"));
//        
        UsuarioJpaController jpaUsuario = new UsuarioJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
//        jpaUsuario.create(new Usuario("usuario2", "usuario", jpaPerfil.findPerfil(222)));
        
        EmpleadoJpaController jpaEmpleado = new EmpleadoJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        jpaEmpleado.create(new Empleado(24, "Efrain2", "Vega2", "Morua2", null, null, null, "efrainvega@gmail.com", 10000.0, null, jpaUsuario.findUsuario("usuario2")));
        
//        System.out.println(jpaEmpleado.findEmpleadoEntities());
        
    }
}
