/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author efrai
 */
@Entity
@Table(name = "empleado")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Empleado.findAll", query = "SELECT e FROM Empleado e"),
    @NamedQuery(name = "Empleado.findByCedula", query = "SELECT e FROM Empleado e WHERE e.cedula = :cedula"),
    @NamedQuery(name = "Empleado.findByNombre", query = "SELECT e FROM Empleado e WHERE e.nombre = :nombre"),
    @NamedQuery(name = "Empleado.findByPrimerApellido", query = "SELECT e FROM Empleado e WHERE e.primerApellido = :primerApellido"),
    @NamedQuery(name = "Empleado.findBySegundoApellido", query = "SELECT e FROM Empleado e WHERE e.segundoApellido = :segundoApellido"),
    @NamedQuery(name = "Empleado.findByFechaIngreso", query = "SELECT e FROM Empleado e WHERE e.fechaIngreso = :fechaIngreso"),
    @NamedQuery(name = "Empleado.findByTelefonoFijo", query = "SELECT e FROM Empleado e WHERE e.telefonoFijo = :telefonoFijo"),
    @NamedQuery(name = "Empleado.findByTelefonoMovil", query = "SELECT e FROM Empleado e WHERE e.telefonoMovil = :telefonoMovil"),
    @NamedQuery(name = "Empleado.findByCorreoInstitucional", query = "SELECT e FROM Empleado e WHERE e.correoInstitucional = :correoInstitucional"),
    @NamedQuery(name = "Empleado.findBySalarioBase", query = "SELECT e FROM Empleado e WHERE e.salarioBase = :salarioBase"),
    @NamedQuery(name = "Empleado.findByAreaTrabajo", query = "SELECT e FROM Empleado e WHERE e.areaTrabajo = :areaTrabajo")})
public class Empleado implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "cedula")
    private Integer cedula;
    @Basic(optional = false)
    @Column(name = "nombre")
    private String nombre;
    @Basic(optional = false)
    @Column(name = "primer_apellido")
    private String primerApellido;
    @Basic(optional = false)
    @Column(name = "segundo_apellido")
    private String segundoApellido;
    @Column(name = "fecha_ingreso")
    @Temporal(TemporalType.TIMESTAMP)
    private Date fechaIngreso;
    @Column(name = "telefono_fijo")
    private String telefonoFijo;
    @Column(name = "telefono_movil")
    private String telefonoMovil;
    @Column(name = "correo_institucional")
    private String correoInstitucional;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "salario_base")
    private Double salarioBase;
    @Column(name = "area_trabajo")
    private String areaTrabajo;
    @JoinColumn(name = "usuario_nombre_usuario", referencedColumnName = "nombre_usuario")
    @ManyToOne(optional = false)
    private Usuario usuarioNombreUsuario;
    public static final String[] ETIQUETAS_EMPLEADOS = {"Cédula", "Nombre", "Primer Apellido", "Segundo Apellido", "Area de Desempeño"};
    public static final String[] ETIQUETAS_EMPLEADOS_CONSULTA = {"Fecha de Ingreso", "Salario Base", "Telefono Fijo", "Telefono Movil", "Correo Institucional"};
    
    public Empleado() {
    }

    public Empleado(Integer cedula, String nombre, String primerApellido, String segundoApellido, Date fechaIngreso, String telefonoFijo, String telefonoMovil, String correoInstitucional, Double salarioBase, String areaTrabajo, Usuario usuarioNombreUsuario) {
        this.cedula = cedula;
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.fechaIngreso = fechaIngreso;
        this.telefonoFijo = telefonoFijo;
        this.telefonoMovil = telefonoMovil;
        this.correoInstitucional = correoInstitucional;
        this.salarioBase = salarioBase;
        this.areaTrabajo = areaTrabajo;
        this.usuarioNombreUsuario = usuarioNombreUsuario;
    }
    
    public String setDatosEmpleado(int indice) {
        switch (indice) {
            case 0:
                return this.getCedula().toString();
            case 1:
                return this.getNombre();
            case 2:
                return this.getPrimerApellido();
            case 3:
                return this.getSegundoApellido();
            case 4:
                return this.getAreaTrabajo();
        }
        return null;
    }
    
        public String setDatosEmpleadoConsulta(int indice) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);
        
        switch (indice) {
            case 0:
                return simpleDateFormat.format(this.getFechaIngreso()).toString();
            case 1:
                return this.getSalarioBase().toString();
            case 2:
                return this.getTelefonoFijo();
            case 3:
                return this.getTelefonoMovil();
            case 4:
                return this.getCorreoInstitucional();
        }
        return null;
    }
    
    public Integer getCedula() {
        return cedula;
    }

    public void setCedula(Integer cedula) {
        this.cedula = cedula;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public Date getFechaIngreso() {
        return fechaIngreso;
    }

    public void setFechaIngreso(Date fechaIngreso) {
        this.fechaIngreso = fechaIngreso;
    }

    public String getTelefonoFijo() {
        return telefonoFijo;
    }

    public void setTelefonoFijo(String telefonoFijo) {
        this.telefonoFijo = telefonoFijo;
    }

    public String getTelefonoMovil() {
        return telefonoMovil;
    }

    public void setTelefonoMovil(String telefonoMovil) {
        this.telefonoMovil = telefonoMovil;
    }

    public String getCorreoInstitucional() {
        return correoInstitucional;
    }

    public void setCorreoInstitucional(String correoInstitucional) {
        this.correoInstitucional = correoInstitucional;
    }

    public Double getSalarioBase() {
        return salarioBase;
    }

    public void setSalarioBase(Double salarioBase) {
        this.salarioBase = salarioBase;
    }

    public String getAreaTrabajo() {
        return areaTrabajo;
    }

    public void setAreaTrabajo(String areaTrabajo) {
        this.areaTrabajo = areaTrabajo;
    }

    public Usuario getUsuarioNombreUsuario() {
        return usuarioNombreUsuario;
    }

    public void setUsuarioNombreUsuario(Usuario usuarioNombreUsuario) {
        this.usuarioNombreUsuario = usuarioNombreUsuario;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (cedula != null ? cedula.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Empleado)) {
            return false;
        }
        Empleado other = (Empleado) object;
        if ((this.cedula == null && other.cedula != null) || (this.cedula != null && !this.cedula.equals(other.cedula))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Empleado{" + "cedula=" + cedula + ", nombre=" + nombre + ", primerApellido=" + primerApellido + ", segundoApellido=" + segundoApellido + '}';
    }
}
