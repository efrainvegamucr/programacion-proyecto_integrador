/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author efrai
 */
@Entity
@Table(name = "modulo")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Modulo.findAll", query = "SELECT m FROM Modulo m"),
    @NamedQuery(name = "Modulo.findByCodigoModulo", query = "SELECT m FROM Modulo m WHERE m.codigoModulo = :codigoModulo"),
    @NamedQuery(name = "Modulo.findByDescripcion", query = "SELECT m FROM Modulo m WHERE m.descripcion = :descripcion"),
    @NamedQuery(name = "Modulo.findByNombreModulo", query = "SELECT m FROM Modulo m WHERE m.nombreModulo = :nombreModulo")})
public class Modulo implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "codigo_modulo")
    private Integer codigoModulo;
    @Column(name = "descripcion")
    private String descripcion;
    @Column(name = "nombre_modulo")
    private String nombreModulo;
    @JoinTable(name = "perfil_has_modulo", joinColumns = {
        @JoinColumn(name = "modulo_codigo_modulo", referencedColumnName = "codigo_modulo")}, inverseJoinColumns = {
        @JoinColumn(name = "perfil_codigo", referencedColumnName = "codigo")})
    @ManyToMany
    private List<Perfil> perfilList;

    public Modulo() {
    }

    public Modulo(Integer codigoModulo) {
        this.codigoModulo = codigoModulo;
    }

    public Modulo(Integer codigoModulo, String descripcion, String nombreModulo) {
        this.codigoModulo = codigoModulo;
        this.descripcion = descripcion;
        this.nombreModulo = nombreModulo;
    }

    public Integer getCodigoModulo() {
        return codigoModulo;
    }

    public void setCodigoModulo(Integer codigoModulo) {
        this.codigoModulo = codigoModulo;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getNombreModulo() {
        return nombreModulo;
    }

    public void setNombreModulo(String nombreModulo) {
        this.nombreModulo = nombreModulo;
    }

    @XmlTransient
    public List<Perfil> getPerfilList() {
        return perfilList;
    }

    public void setPerfilList(List<Perfil> perfilList) {
        this.perfilList = perfilList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (codigoModulo != null ? codigoModulo.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Modulo)) {
            return false;
        }
        Modulo other = (Modulo) object;
        if ((this.codigoModulo == null && other.codigoModulo != null) || (this.codigoModulo != null && !this.codigoModulo.equals(other.codigoModulo))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "Modulo{" + "codigoModulo=" + codigoModulo + ", descripcion=" + descripcion + ", nombreModulo=" + nombreModulo + '}';
    }
}
