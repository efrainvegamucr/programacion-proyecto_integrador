/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Modelo.exceptions.IllegalOrphanException;
import Modelo.exceptions.NonexistentEntityException;
import Modelo.exceptions.PreexistingEntityException;
import java.io.Serializable;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.ArrayList;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;

/**
 *
 * @author efrai
 */
public class PerfilJpaController implements Serializable {

    public PerfilJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public String[][] getDatosTabla() {
        String[][] datos = new String[findPerfilEntities().size()][Perfil.ETIQUETAS_PERFILES.length];
        for (int indice = 0; indice < findPerfilEntities().size(); indice++) {
            for (int atributo = 0; atributo < datos[indice].length; atributo++) {
                datos[indice][atributo] = this.findPerfilEntities().get(indice).setDatosPerfil(atributo);
            }
        }
        return datos;
    }
        
    public void create(Perfil perfil) throws PreexistingEntityException, Exception {
        if (perfil.getModuloList() == null) {
            perfil.setModuloList(new ArrayList<Modulo>());
        }
        if (perfil.getUsuarioList() == null) {
            perfil.setUsuarioList(new ArrayList<Usuario>());
        }
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            List<Modulo> attachedModuloList = new ArrayList<Modulo>();
            for (Modulo moduloListModuloToAttach : perfil.getModuloList()) {
                moduloListModuloToAttach = em.getReference(moduloListModuloToAttach.getClass(), moduloListModuloToAttach.getCodigoModulo());
                attachedModuloList.add(moduloListModuloToAttach);
            }
            perfil.setModuloList(attachedModuloList);
            List<Usuario> attachedUsuarioList = new ArrayList<Usuario>();
            for (Usuario usuarioListUsuarioToAttach : perfil.getUsuarioList()) {
                usuarioListUsuarioToAttach = em.getReference(usuarioListUsuarioToAttach.getClass(), usuarioListUsuarioToAttach.getNombreUsuario());
                attachedUsuarioList.add(usuarioListUsuarioToAttach);
            }
            perfil.setUsuarioList(attachedUsuarioList);
            em.persist(perfil);
            for (Modulo moduloListModulo : perfil.getModuloList()) {
                moduloListModulo.getPerfilList().add(perfil);
                moduloListModulo = em.merge(moduloListModulo);
            }
            for (Usuario usuarioListUsuario : perfil.getUsuarioList()) {
                Perfil oldPerfilCodigoOfUsuarioListUsuario = usuarioListUsuario.getPerfilCodigo();
                usuarioListUsuario.setPerfilCodigo(perfil);
                usuarioListUsuario = em.merge(usuarioListUsuario);
                if (oldPerfilCodigoOfUsuarioListUsuario != null) {
                    oldPerfilCodigoOfUsuarioListUsuario.getUsuarioList().remove(usuarioListUsuario);
                    oldPerfilCodigoOfUsuarioListUsuario = em.merge(oldPerfilCodigoOfUsuarioListUsuario);
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findPerfil(perfil.getCodigo()) != null) {
                throw new PreexistingEntityException("Perfil " + perfil + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Perfil perfil) throws IllegalOrphanException, NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Perfil persistentPerfil = em.find(Perfil.class, perfil.getCodigo());
//            List<Modulo> moduloListOld = persistentPerfil.getModuloList();
//            List<Modulo> moduloListNew = perfil.getModuloList();
            List<Usuario> usuarioListOld = persistentPerfil.getUsuarioList();
            List<Usuario> usuarioListNew = perfil.getUsuarioList();
            List<String> illegalOrphanMessages = null;
            for (Usuario usuarioListOldUsuario : usuarioListOld) {
                if (!usuarioListNew.contains(usuarioListOldUsuario)) {
                    if (illegalOrphanMessages == null) {
                        illegalOrphanMessages = new ArrayList<String>();
                    }
                    illegalOrphanMessages.add("You must retain Usuario " + usuarioListOldUsuario + " since its perfilCodigo field is not nullable.");
                }
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
//            List<Modulo> attachedModuloListNew = new ArrayList<Modulo>();
//            for (Modulo moduloListNewModuloToAttach : moduloListNew) {
//                moduloListNewModuloToAttach = em.getReference(moduloListNewModuloToAttach.getClass(), moduloListNewModuloToAttach.getCodigoModulo());
//                attachedModuloListNew.add(moduloListNewModuloToAttach);
//            }
//            moduloListNew = attachedModuloListNew;
//            perfil.setModuloList(moduloListNew);
            List<Usuario> attachedUsuarioListNew = new ArrayList<Usuario>();
            for (Usuario usuarioListNewUsuarioToAttach : usuarioListNew) {
                usuarioListNewUsuarioToAttach = em.getReference(usuarioListNewUsuarioToAttach.getClass(), usuarioListNewUsuarioToAttach.getNombreUsuario());
                attachedUsuarioListNew.add(usuarioListNewUsuarioToAttach);
            }
            usuarioListNew = attachedUsuarioListNew;
            perfil.setUsuarioList(usuarioListNew);
            perfil = em.merge(perfil);
//            for (Modulo moduloListOldModulo : moduloListOld) {
//                if (!moduloListNew.contains(moduloListOldModulo)) {
//                    moduloListOldModulo.getPerfilList().remove(perfil);
//                    moduloListOldModulo = em.merge(moduloListOldModulo);
//                }
//            }
//            for (Modulo moduloListNewModulo : moduloListNew) {
//                if (!moduloListOld.contains(moduloListNewModulo)) {
//                    moduloListNewModulo.getPerfilList().add(perfil);
//                    moduloListNewModulo = em.merge(moduloListNewModulo);
//                }
//            }
            for (Usuario usuarioListNewUsuario : usuarioListNew) {
                if (!usuarioListOld.contains(usuarioListNewUsuario)) {
                    Perfil oldPerfilCodigoOfUsuarioListNewUsuario = usuarioListNewUsuario.getPerfilCodigo();
                    usuarioListNewUsuario.setPerfilCodigo(perfil);
                    usuarioListNewUsuario = em.merge(usuarioListNewUsuario);
                    if (oldPerfilCodigoOfUsuarioListNewUsuario != null && !oldPerfilCodigoOfUsuarioListNewUsuario.equals(perfil)) {
                        oldPerfilCodigoOfUsuarioListNewUsuario.getUsuarioList().remove(usuarioListNewUsuario);
                        oldPerfilCodigoOfUsuarioListNewUsuario = em.merge(oldPerfilCodigoOfUsuarioListNewUsuario);
                    }
                }
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = perfil.getCodigo();
                if (findPerfil(id) == null) {
                    throw new NonexistentEntityException("The perfil with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws IllegalOrphanException, NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Perfil perfil;
            try {
                perfil = em.getReference(Perfil.class, id);
                perfil.getCodigo();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The perfil with id " + id + " no longer exists.", enfe);
            }
            List<String> illegalOrphanMessages = null;
            List<Usuario> usuarioListOrphanCheck = perfil.getUsuarioList();
            for (Usuario usuarioListOrphanCheckUsuario : usuarioListOrphanCheck) {
                if (illegalOrphanMessages == null) {
                    illegalOrphanMessages = new ArrayList<String>();
                }
                illegalOrphanMessages.add("This Perfil (" + perfil + ") cannot be destroyed since the Usuario " + usuarioListOrphanCheckUsuario + " in its usuarioList field has a non-nullable perfilCodigo field.");
            }
            if (illegalOrphanMessages != null) {
                throw new IllegalOrphanException(illegalOrphanMessages);
            }
            List<Modulo> moduloList = perfil.getModuloList();
            for (Modulo moduloListModulo : moduloList) {
                moduloListModulo.getPerfilList().remove(perfil);
                moduloListModulo = em.merge(moduloListModulo);
            }
            em.remove(perfil);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Perfil> findPerfilEntities() {
        return findPerfilEntities(true, -1, -1);
    }

    public List<Perfil> findPerfilEntities(int maxResults, int firstResult) {
        return findPerfilEntities(false, maxResults, firstResult);
    }

    private List<Perfil> findPerfilEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Perfil.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Perfil findPerfil(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Perfil.class, id);
        } finally {
            em.close();
        }
    }

    public int getPerfilCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Perfil> rt = cq.from(Perfil.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
