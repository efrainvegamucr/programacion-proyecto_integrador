/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Modelo;

import Modelo.exceptions.NonexistentEntityException;
import Modelo.exceptions.PreexistingEntityException;
import java.io.Serializable;
import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Query;
import javax.persistence.EntityNotFoundException;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;

/**
 *
 * @author efrai
 */
public class EmpleadoJpaController implements Serializable {

    public EmpleadoJpaController(EntityManagerFactory emf) {
        this.emf = emf;
    }
    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
    
    public String[][] getDatosTabla() {
        String[][] datos = new String[findEmpleadoEntities().size()][Empleado.ETIQUETAS_EMPLEADOS.length];
        for (int indice = 0; indice < findEmpleadoEntities().size(); indice++) {
            for (int atributo = 0; atributo < datos[indice].length; atributo++) {
                datos[indice][atributo] = this.findEmpleadoEntities().get(indice).setDatosEmpleado(atributo);
            }
        }
        return datos;
    }
    
        public String[][] getDatosTablaConsulta(int cedula) {
        String[][] datos = new String[findEmpleadoEntities().size()][Empleado.ETIQUETAS_EMPLEADOS_CONSULTA.length];
        for (int indice = 0; indice < findEmpleadoEntities().size(); indice++) {
            if(this.findEmpleadoEntities().get(indice).getCedula() == cedula) {
                datos = new String[1][Empleado.ETIQUETAS_EMPLEADOS_CONSULTA.length];
                for (int atributo = 0; atributo < datos[0].length; atributo++) {
                        datos[0][atributo] = this.findEmpleadoEntities().get(indice).setDatosEmpleadoConsulta(atributo);
                }
            break;
            }
        }
        return datos;
    }
    
    public void create(Empleado empleado) throws PreexistingEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Usuario usuarioNombreUsuario = empleado.getUsuarioNombreUsuario();
            if (usuarioNombreUsuario != null) {
                usuarioNombreUsuario = em.getReference(usuarioNombreUsuario.getClass(), usuarioNombreUsuario.getNombreUsuario());
                empleado.setUsuarioNombreUsuario(usuarioNombreUsuario);
            }
            em.persist(empleado);
            if (usuarioNombreUsuario != null) {
                usuarioNombreUsuario.getEmpleadoList().add(empleado);
                usuarioNombreUsuario = em.merge(usuarioNombreUsuario);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            if (findEmpleado(empleado.getCedula()) != null) {
                throw new PreexistingEntityException("Empleado " + empleado + " already exists.", ex);
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void edit(Empleado empleado) throws NonexistentEntityException, Exception {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado persistentEmpleado = em.find(Empleado.class, empleado.getCedula());
            Usuario usuarioNombreUsuarioOld = persistentEmpleado.getUsuarioNombreUsuario();
            Usuario usuarioNombreUsuarioNew = empleado.getUsuarioNombreUsuario();
            if (usuarioNombreUsuarioNew != null) {
                usuarioNombreUsuarioNew = em.getReference(usuarioNombreUsuarioNew.getClass(), usuarioNombreUsuarioNew.getNombreUsuario());
                empleado.setUsuarioNombreUsuario(usuarioNombreUsuarioNew);
            }
            empleado = em.merge(empleado);
            if (usuarioNombreUsuarioOld != null && !usuarioNombreUsuarioOld.equals(usuarioNombreUsuarioNew)) {
                usuarioNombreUsuarioOld.getEmpleadoList().remove(empleado);
                usuarioNombreUsuarioOld = em.merge(usuarioNombreUsuarioOld);
            }
            if (usuarioNombreUsuarioNew != null && !usuarioNombreUsuarioNew.equals(usuarioNombreUsuarioOld)) {
                usuarioNombreUsuarioNew.getEmpleadoList().add(empleado);
                usuarioNombreUsuarioNew = em.merge(usuarioNombreUsuarioNew);
            }
            em.getTransaction().commit();
        } catch (Exception ex) {
            String msg = ex.getLocalizedMessage();
            if (msg == null || msg.length() == 0) {
                Integer id = empleado.getCedula();
                if (findEmpleado(id) == null) {
                    throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.");
                }
            }
            throw ex;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public void destroy(Integer id) throws NonexistentEntityException {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            Empleado empleado;
            try {
                empleado = em.getReference(Empleado.class, id);
                empleado.getCedula();
            } catch (EntityNotFoundException enfe) {
                throw new NonexistentEntityException("The empleado with id " + id + " no longer exists.", enfe);
            }
            Usuario usuarioNombreUsuario = empleado.getUsuarioNombreUsuario();
            if (usuarioNombreUsuario != null) {
                usuarioNombreUsuario.getEmpleadoList().remove(empleado);
                usuarioNombreUsuario = em.merge(usuarioNombreUsuario);
            }
            em.remove(empleado);
            em.getTransaction().commit();
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    public List<Empleado> findEmpleadoEntities() {
        return findEmpleadoEntities(true, -1, -1);
    }

    public List<Empleado> findEmpleadoEntities(int maxResults, int firstResult) {
        return findEmpleadoEntities(false, maxResults, firstResult);
    }

    private List<Empleado> findEmpleadoEntities(boolean all, int maxResults, int firstResult) {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            cq.select(cq.from(Empleado.class));
            Query q = em.createQuery(cq);
            if (!all) {
                q.setMaxResults(maxResults);
                q.setFirstResult(firstResult);
            }
            return q.getResultList();
        } finally {
            em.close();
        }
    }

    public Empleado findEmpleado(Integer id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Empleado.class, id);
        } finally {
            em.close();
        }
    }

    public int getEmpleadoCount() {
        EntityManager em = getEntityManager();
        try {
            CriteriaQuery cq = em.getCriteriaBuilder().createQuery();
            Root<Empleado> rt = cq.from(Empleado.class);
            cq.select(em.getCriteriaBuilder().count(rt));
            Query q = em.createQuery(cq);
            return ((Long) q.getSingleResult()).intValue();
        } finally {
            em.close();
        }
    }
    
}
