/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.EmpleadoJpaController;
import Modelo.Perfil;
import Modelo.PerfilJpaController;
import Modelo.Usuario;
import Modelo.UsuarioJpaController;
import Vista.GUIUsuarios;
import Vista.PanelUsuarios;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;

/**
 *
 * @author efrai
 */
public class ControladorUsuarios implements ActionListener, MouseListener {

    private EmpleadoJpaController jpaEmpleado;

    private Usuario usuario;
    private UsuarioJpaController jpaUsuario;

    private Perfil perfil;
    private PerfilJpaController jpaPerfil;

    private GUIUsuarios guiUsuarios;
    private PanelUsuarios panelUsuarios;

    public ControladorUsuarios(GUIUsuarios guiUsuarios, PanelUsuarios panelUsuarios) {
        this.guiUsuarios = guiUsuarios;
        this.panelUsuarios = panelUsuarios;

        this.jpaEmpleado = new EmpleadoJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        this.jpaUsuario = new UsuarioJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        this.jpaPerfil = new PerfilJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        this.panelUsuarios.setDatosTabla(jpaUsuario.getDatosTabla(), Usuario.ETIQUETAS_USUARIOS);
        this.panelUsuarios.cambiarBotones(true);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();

        switch (valorLeido) {
            case "Agregar":
                try {
                    if (!panelUsuarios.getTxtCodigoPerfil().equals("") && !panelUsuarios.getTxtaDescripcion().equals("") && !panelUsuarios.getTxtUsuario().equals("") && !panelUsuarios.getTxtContraseña().equals("")) {
                        if (panelUsuarios.getTxtCodigoPerfil().matches("[+-]?\\d*(\\.\\d+)?")) {
                            if (jpaPerfil.findPerfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil())) == null) {
                                if (jpaUsuario.findUsuario(panelUsuarios.getTxtUsuario()) == null) {
                                    jpaPerfil.create(new Perfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil()), panelUsuarios.getJcbAreaDesempeño(), panelUsuarios.getTxtaDescripcion()));
                                    jpaUsuario.create(new Usuario(panelUsuarios.getTxtUsuario(), panelUsuarios.getTxtContraseña(), jpaPerfil.findPerfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil()))));
                                    panelUsuarios.setDatosTabla(jpaUsuario.getDatosTabla(), Usuario.ETIQUETAS_USUARIOS);
                                    panelUsuarios.limpiar();
                                } else {
                                    guiUsuarios.mostrarMensaje("Usuario no válido\nHay uno existente.");
                                }
                            } else {
                                guiUsuarios.mostrarMensaje("Codigo Perfil no válido\nHay uno existente.");
                            }
                        } else {
                            guiUsuarios.mostrarMensaje("Codigo Perfil no válido\nIngrese solo numeros.");
                        }
                    } else {
                        guiUsuarios.mostrarMensaje("Datos no válidos\nIngrese todos los campos.");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ControladorUsuarios.class.getName()).log(Level.SEVERE, null, ex);
                }
            break;

            case "Modificar":
                try {
                    if (!panelUsuarios.getTxtCodigoPerfil().equals("") && !panelUsuarios.getTxtaDescripcion().equals("") && !panelUsuarios.getTxtUsuario().equals("") && !panelUsuarios.getTxtContraseña().equals("")) {
                        if (panelUsuarios.getTxtCodigoPerfil().matches("[+-]?\\d*(\\.\\d+)?")) {
                            if (jpaPerfil.findPerfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil())) != null) {
                                if (jpaUsuario.findUsuario(panelUsuarios.getTxtUsuario()) != null) {
                                    Perfil perfilNuevo = new Perfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil()), panelUsuarios.getJcbAreaDesempeño(), panelUsuarios.getTxtaDescripcion());
                                    perfilNuevo.setUsuarioList(perfil.getUsuarioList());
                                    jpaPerfil.edit(perfilNuevo);
                                        
                                    Usuario usuarioNuevo = new Usuario(panelUsuarios.getTxtUsuario(), panelUsuarios.getTxtContraseña(), jpaPerfil.findPerfil(Integer.parseInt(panelUsuarios.getTxtCodigoPerfil())));
                                    usuarioNuevo.setEmpleadoList(usuario.getEmpleadoList());
                                    jpaUsuario.edit(usuarioNuevo);
                                    
                                    panelUsuarios.cambiarBotones(true);
                                    panelUsuarios.limpiar();
                                } else {
                                    guiUsuarios.mostrarMensaje("Usuario no válido\nNo se ha encontrado en el registro.");
                                }
                            } else {
                                guiUsuarios.mostrarMensaje("Codigo Perfil no válido\nNo se ha encontrado en el registro.");
                            }
                        } else {
                            guiUsuarios.mostrarMensaje("Codigo Perfil no válido\nIngrese solo numeros.");
                        }
                    } else {
                        guiUsuarios.mostrarMensaje("Datos no válidos\nIngrese todos los campos.");
                    }
                } catch (Exception ex) {
                    Logger.getLogger(ControladorUsuarios.class.getName()).log(Level.SEVERE, null, ex);
                }
            panelUsuarios.setDatosTabla(jpaUsuario.getDatosTabla(), Usuario.ETIQUETAS_USUARIOS);
            break;

            case "Eliminar":

                panelUsuarios.setDatosTabla(jpaUsuario.getDatosTabla(), Usuario.ETIQUETAS_USUARIOS);
                break;

            case "Salir":
                guiUsuarios.dispose();
                guiUsuarios.guiOpciones.setVisible(true);
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        usuario = jpaUsuario.findUsuario(panelUsuarios.getFila());
        perfil = jpaPerfil.findPerfil(usuario.getPerfilCodigo().getCodigo());

        panelUsuarios.setTxtCodigoPerfil(usuario.getPerfilCodigo().getCodigo().toString());
        panelUsuarios.setTxtaDescripcion(usuario.getPerfilCodigo().getDescripcion());
        panelUsuarios.setTxtUsuario(usuario.getNombreUsuario());
        panelUsuarios.setTxtContraseña(usuario.getContraseña());
        
        panelUsuarios.cambiarBotones(false);
    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
