/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Empleado;
import Modelo.EmpleadoJpaController;
import Modelo.UsuarioJpaController;
import Vista.GUIEmpleados;
import Vista.GUIReporteEmpleados;
import Vista.PanelEmpleados;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.persistence.Persistence;

/**
 *
 * @author juare
 */
public class ControladorEmpleados implements ActionListener, MouseListener {

    private Empleado empleado;
    private EmpleadoJpaController jpaEmpleado;
    private UsuarioJpaController jpaUsuario;
    private GUIEmpleados guiEmpleados;
    private PanelEmpleados panelEmpleados;
    private GUIReporteEmpleados guiReporteEmpleados;

    public ControladorEmpleados(GUIEmpleados guiEmpleados, PanelEmpleados panelEmpleados) {
        this.guiEmpleados = guiEmpleados;
        this.panelEmpleados = panelEmpleados;
        this.jpaEmpleado = new EmpleadoJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        this.jpaUsuario = new UsuarioJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
        this.guiEmpleados.setComboUsuarios(jpaUsuario.findUsuarioEntities());
        panelEmpleados.setDatosTabla(jpaEmpleado.getDatosTabla(), Empleado.ETIQUETAS_EMPLEADOS);
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();

        switch (valorLeido) {
            case "Agregar":
                try {
                if (!panelEmpleados.getTxtNombre().equals("") && !panelEmpleados.getTxtPrimerApellido().equals("") && !panelEmpleados.getTxtSegundoApellido().equals("")
                        && panelEmpleados.getTxtCedula() != 0 && panelEmpleados.getTxtFechaIngreso() != null && panelEmpleados.getTxtSalarioBase() != 0.0 && panelEmpleados.getTxtTelefonoFijo().equals("")
                        && panelEmpleados.getTxtTelefonoMovil().equals("") && panelEmpleados.getTxtCorreo().equals("") && panelEmpleados.getjCArea_Desempeño().equals("-") && panelEmpleados.getjCAisgnar_Usuario().equals("-")) {
                    jpaEmpleado.create(
                            new Empleado(
                                    Integer.valueOf(panelEmpleados.getTxtCedula()), panelEmpleados.getTxtNombre(),
                                    panelEmpleados.getTxtPrimerApellido(), panelEmpleados.getTxtSegundoApellido(),
                                    new Date(), panelEmpleados.getTxtTelefonoFijo(), panelEmpleados.getTxtTelefonoMovil(),
                                    panelEmpleados.getTxtCorreo(), Double.valueOf(panelEmpleados.getTxtSalarioBase()), panelEmpleados.getjCArea_Desempeño(),
                                    jpaUsuario.findUsuario(panelEmpleados.getjCAisgnar_Usuario())
                            )
                    );
                } else {
                    guiEmpleados.mostrarMensaje("Hola debes de llenar todas las casillas para agregar a un empleado");
                }
                panelEmpleados.limpiar(); 
                panelEmpleados.setDatosTabla(jpaEmpleado.getDatosTabla(), Empleado.ETIQUETAS_EMPLEADOS);
            } catch (Exception ex) {
            }
            break;

            case "Consultar":
                guiReporteEmpleados = new GUIReporteEmpleados(this);
                guiReporteEmpleados.setVisible(true);
                guiReporteEmpleados.setDatosTabla(jpaEmpleado.getDatosTablaConsulta(empleado.getCedula()), Empleado.ETIQUETAS_EMPLEADOS_CONSULTA);
                break;

            case "Modificar":
                Empleado empleadoNuevo = new Empleado(
                        Integer.valueOf(panelEmpleados.getTxtCedula()), panelEmpleados.getTxtNombre(),
                        panelEmpleados.getTxtPrimerApellido(), panelEmpleados.getTxtSegundoApellido(),
                        new Date(), panelEmpleados.getTxtTelefonoFijo(), panelEmpleados.getTxtTelefonoMovil(),
                        panelEmpleados.getTxtCorreo(), Double.valueOf(panelEmpleados.getTxtSalarioBase()), panelEmpleados.getjCArea_Desempeño(),
                        jpaUsuario.findUsuario(panelEmpleados.getjCAisgnar_Usuario())
                );
                try {
                    jpaEmpleado.edit(empleadoNuevo);
                } catch (Exception ex) {
                    Logger.getLogger(ControladorEmpleados.class.getName()).log(Level.SEVERE, null, ex);
                }
                panelEmpleados.setDatosTabla(jpaEmpleado.getDatosTabla(), Empleado.ETIQUETAS_EMPLEADOS);
                break;

            case "Eliminar":

                break;

            case "Salir":
                guiEmpleados.dispose();
                guiEmpleados.guiOpciones.setVisible(true);
                break;
        }
    }

    @Override
    public void mouseClicked(MouseEvent e) {
        String pattern = "yyyy-MM-dd";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern);

        empleado = jpaEmpleado.findEmpleado(Integer.parseInt(panelEmpleados.getFila()));

        panelEmpleados.setTxtCedula(empleado.getCedula());
        panelEmpleados.setTxtNombre(empleado.getNombre());
        panelEmpleados.setTxtPrimerApellido(empleado.getPrimerApellido());
        panelEmpleados.setTxtSegundoApellido(empleado.getSegundoApellido());

        panelEmpleados.setTxtFechaIngreso(simpleDateFormat.format(empleado.getFechaIngreso()));
        panelEmpleados.setTxtSalarioBase(empleado.getSalarioBase());
        panelEmpleados.setTxtTelefonoFijo(empleado.getTelefonoFijo());
        panelEmpleados.setTxtTelefonoMovil(empleado.getTelefonoMovil());
        panelEmpleados.setTxtCorreo(empleado.getCorreoInstitucional());

    }

    @Override
    public void mousePressed(MouseEvent e) {
    }

    @Override
    public void mouseReleased(MouseEvent e) {
    }

    @Override
    public void mouseEntered(MouseEvent e) {
    }

    @Override
    public void mouseExited(MouseEvent e) {
    }
}
