/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Usuario;
import Modelo.UsuarioJpaController;
import Vista.GUIAutenticacion;
import Vista.GUIOpciones;
import Vista.PanelAutenticacion;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.persistence.Persistence;
import javax.swing.JOptionPane;

/**
 *
 * @author efrai
 */
public class ControladorAutenticacion implements ActionListener {

    private GUIAutenticacion guiAutenticacion;
    private PanelAutenticacion panelAutenticacion;
    private GUIOpciones guiOpciones;
    private Usuario usuario;
    private UsuarioJpaController jpaUsuario;

    public ControladorAutenticacion(GUIAutenticacion guiAutenticacion, PanelAutenticacion panelAutenticacion) {
        this.guiAutenticacion = guiAutenticacion;
        this.panelAutenticacion = panelAutenticacion;
        this.jpaUsuario = new UsuarioJpaController(Persistence.createEntityManagerFactory("Proyecto_Integrador"));
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();

        switch (valorLeido) {
            case "Ingresar":
                try {
                if (!panelAutenticacion.getTxtNombreUsuario().equals("")) {
                    if (panelAutenticacion.getTxtjPContraseña().length != 0) {
                        if (jpaUsuario.findUsuario(panelAutenticacion.getTxtNombreUsuario()) != null) {
                            if(jpaUsuario.findUsuario(panelAutenticacion.getTxtNombreUsuario()).getContraseña().equals(new String(panelAutenticacion.getTxtjPContraseña()))) {    
                                GUIOpciones guiOpciones = new GUIOpciones(guiAutenticacion, jpaUsuario.findUsuario(panelAutenticacion.getTxtNombreUsuario()));
                                guiOpciones.setVisible(true);
                                guiAutenticacion.setVisible(false);
                                panelAutenticacion.limpiar();
                            } else {
                                guiAutenticacion.mostrarMensaje("Contraseña no válida\nIngrese nuevamente.");
                            }
                        } else {
                            guiAutenticacion.mostrarMensaje("Usuario no válido\nIngrese nuevamente.");
                        }
                    } else {
                        guiAutenticacion.mostrarMensaje("Contraseña está vacio\nIngresélo por favor.");
                    }
                } else {
                    guiAutenticacion.mostrarMensaje("Usuario está vacio\nIngresélo por favor.");
                }
            } catch (Exception ex) {
            }
            break;
            case "Salir":
                guiAutenticacion.dispose();
                break;
        }
    }

}
