/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package Controlador;

import Modelo.Empleado;
import Modelo.EmpleadoJpaController;
import Vista.GUIEmpleados;
import Vista.GUIOpciones;
import Vista.GUIUsuarios;
import Vista.PanelOpciones;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class ControladorOpciones implements ActionListener {

    private GUIOpciones guiOpciones;
    private PanelOpciones panelOpciones;
    private GUIEmpleados guiEmpleados;
    private GUIUsuarios guiUsuarios;

    public ControladorOpciones(GUIOpciones guiOpciones, PanelOpciones panelOpciones) {
        this.guiOpciones = guiOpciones;
        this.panelOpciones = panelOpciones;
    }

    @Override
    public void actionPerformed(ActionEvent evento) {
        String valorLeido = evento.getActionCommand();

        switch (valorLeido) {
            case "Mantenimiento de Empleados":
                guiEmpleados = new GUIEmpleados(guiOpciones);
                guiEmpleados.setVisible(true);
                guiOpciones.setVisible(false);
                break;
            case "Mantenimiento de Usuarios":
                guiUsuarios = new GUIUsuarios(guiOpciones);
                guiUsuarios.setVisible(true);
                guiOpciones.setVisible(false);
                break;
            case "Salir":
                guiOpciones.dispose();
                guiOpciones.guiAutenticacion.setVisible(true);
                break;
        }
    }
}
